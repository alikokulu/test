﻿using UnityEngine;
using System;  // required for use of Math.Sign, which works differently than Mathf.Sign

public class Gnome : MonoBehaviour {

    public float horizontalInput;
    public float horizontalThrust;      // the amount of force to apply when moving left or right
    public float jumpThrust;            // the amount of force to apply when pressing the jump button

    public GameObject gnomeGraphics;    // the game object parent where all the graphical elements have been grouped
    public Transform pickAxeHolder;     // location for the pick axe to go after it has been picked up

    public bool isFacingRight;          // for flipping the graphics when changing direction

    Rigidbody2D rigidbody2DComponent;

    void Awake() {
        // get references to components
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {

        // flip graphics if input direction is not the same as the current direction
        if (!isFacingRight && Math.Sign(Input.GetAxis("Horizontal")) == 1) {
            isFacingRight = true;
            gnomeGraphics.transform.localScale = new Vector3(-1, 1, 1);
        } else if (isFacingRight && Math.Sign(Input.GetAxis("Horizontal")) == -1) {
            isFacingRight = false;
            gnomeGraphics.transform.localScale = new Vector3(1, 1, 1);
        }

        // store input
        horizontalInput = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump")) {
            Jump();
        }
    }

    void Jump() {
        rigidbody2DComponent.AddForce(new Vector3(0, jumpThrust), ForceMode2D.Impulse);
    }

    void FixedUpdate() {
       
        if (horizontalInput == 0) return;

        // add horizontal force
        rigidbody2DComponent.AddForce(new Vector3(horizontalInput * horizontalThrust, 0));
    }

    // OnTriggerEnter2D is called when the Collider2D other enters the trigger (2D physics only)
    public void OnTriggerEnter2D(Collider2D other) {

        // destroy pick up items when touched
        if (other.tag == "Pick-Up") {
            // normally, you might add to the player's score or inventory before destroying the game object
            Destroy(other.gameObject);
        }
        // attach the pick axe to the player
        if (other.name == "Pick Axe") {
            other.enabled = false;
            other.transform.parent = pickAxeHolder;
            other.transform.localPosition = Vector3.zero;
        }
    }
}
