﻿using UnityEngine;

public class Rotate : MonoBehaviour {
   
    public float rotationRate;
    // Update is called once per frame
    void Update() {
        transform.Rotate(new Vector3(0, 0, rotationRate * Time.deltaTime));
    }
}
