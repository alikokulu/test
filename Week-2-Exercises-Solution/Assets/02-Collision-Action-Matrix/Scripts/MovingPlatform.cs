﻿using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    public float verticalVelocity;
    public float maxHeight = 3f;
    Vector3 initialPosition;

    Rigidbody2D rigidbody2DComponent;

    void Awake() {
        // get a reference to the rigidbody2D component
        rigidbody2DComponent = GetComponent<Rigidbody2D>();

        // record the initial position of the platform
        initialPosition = transform.position;
    }

    void FixedUpdate() {

        // reset the platform
        //
        // here we do not use MovePosition because that would tell the physics enginge
        // to simulate the next physics steps with the velocity required 
        // to move the platform back to it's initial position
        // within the span of a single fixed update
        //
        // instead we use tranform.position to "teleport" the moving platform back to its
        // initial position

        if (transform.position.y > maxHeight) {
            transform.position = initialPosition;
        }

        // use MovePosition when translating a rigidbody if you would like the Physics engine to handle collisions proplerly with this object
        rigidbody2DComponent.MovePosition(transform.position + new Vector3(0, verticalVelocity) * Time.deltaTime);

    }
}
