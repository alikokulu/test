﻿using UnityEngine;

public class Spaceship : MonoBehaviour {

    public float torque;
    public float thrust;
    public bool doThrust;

    public float laserFiringRatePerSecond = 2;

    public GameObject thrusterFire;
    public Transform lasersParent;
    public Transform leftCannon;
    public Transform rightCannon;

    public GameObject laserPrefab;

    float horizontalInput;
    float timeSinceLastShot;

    Rigidbody2D rigidbody2DComponent;
    AudioSource audioSourceComponent;

    void Awake() {
        Debug.Log("First commit");

        // get references to components
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
        audioSourceComponent = GetComponent<AudioSource>();

        // take a time stamp for measuring the interval between laser shots
        timeSinceLastShot = Time.time;
    }

    void Update() {
        Debug.Log("commit on second branch");
        // always capture input in Update()
        horizontalInput = Input.GetAxis("Horizontal") * 5;
        doThrust = Input.GetButton("Up");

        // show or hide the ship's thruster graphic depending on whether "Up" is pressed or not
        if (doThrust && thrusterFire.activeSelf == false) {
            thrusterFire.SetActive(true);
        } else if (!doThrust && thrusterFire.activeSelf == true) {
            thrusterFire.SetActive(false);
        }

        // fire lasers if shooting interval has elapsed
        if (Input.GetButton("Jump")) {
            if (Time.time - timeSinceLastShot > 1 / laserFiringRatePerSecond) {
                timeSinceLastShot = Time.time;
                FireLasers();

            }
        }
    }

    void FireLasers() {
        // play the laser firing sound
        audioSourceComponent.Play();

        // instantiate new laser prefabs at the cannon positions and rotate them according to the ship's rotation
        GameObject leftLaser = Instantiate(laserPrefab, leftCannon.position, transform.rotation) as GameObject;
        GameObject rightLaser = Instantiate(laserPrefab, rightCannon.position, transform.rotation) as GameObject;

        // make the lasers children of the "Lasers" game object
        leftLaser.transform.parent = lasersParent.transform;
        rightLaser.transform.parent = lasersParent.transform;
    }

    void FixedUpdate() {
        // rotate ship
        rigidbody2DComponent.AddTorque(-horizontalInput * torque);

        // move ship forward
        if (doThrust) {
            float angleInRadians = Mathf.Deg2Rad * transform.localRotation.eulerAngles.z * 2;
            Vector3 forward = new Vector3(Mathf.Cos(angleInRadians), Mathf.Sin(angleInRadians), 0);
            rigidbody2DComponent.AddForce(forward * thrust);
        }
    }

}
