﻿using UnityEngine;

public class LaserTrigger : MonoBehaviour {

    public Transform laser;

    public void OnTriggerEnter2D(Collider2D other) {
        // if the laser overlapped a meteor, remove it from the game
        if (other.tag == "Meteor") {
            Destroy(laser.gameObject);
        }
    }
}
