﻿using UnityEngine;

public class Meteor : MonoBehaviour {

    public int hitPoints = 3;
    public float initialMaxSpeed;
    public float initialMaxAngularVelocity;

    Rigidbody2D rigidbody2DComponent;

    void Awake() {
        // get references to components
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    void Start() {
        // assign the meteor a random initial velocity
        Vector2 randomVelocity = Random.insideUnitCircle * initialMaxSpeed;
        rigidbody2DComponent.velocity = randomVelocity;
        
        // set a random initial angular velocity
        rigidbody2DComponent.angularVelocity = Random.Range(-initialMaxAngularVelocity, initialMaxAngularVelocity);
    }

    void Hit() {
        // remove a hitpoint on each hit
        hitPoints--;
        if (hitPoints <= 0) {
            // remove the game object from the game if the meteor has no more hitpoints
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        // if the meteor overlapped a laser, then the meteor was hit
        if (other.tag == "Laser") {
            Hit();
        }
    }
}
