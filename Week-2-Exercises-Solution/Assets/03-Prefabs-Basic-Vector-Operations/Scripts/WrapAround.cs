﻿using UnityEngine;

public class WrapAround : MonoBehaviour {

    public float wrapThresholdX = 9.4f;
    public float wrapThresholdY = 5.6f;

    Rect bounds;

    Rigidbody2D rigidbody2DComponent;

    void Awake() {
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    void Start() {
        bounds = Rect.MinMaxRect(-wrapThresholdX, -wrapThresholdY, wrapThresholdX, wrapThresholdY);
    }

    // Update is called once per frame
    void Update() {

        // if moving to the right and goes passed the right bounds
        if (transform.position.x > bounds.xMax && rigidbody2DComponent.velocity.x > 0) {
            WraparoundX();
        }
        // if moving to the left and goes passed the left bounds
        else if (transform.position.x < bounds.xMin && rigidbody2DComponent.velocity.x < 0) {
            WraparoundX();
        }

        // if moving up and goes passed the top bounds
        if (transform.position.y > bounds.yMax && rigidbody2DComponent.velocity.y > 0) {
            WraparoundY();
        }
        // if moving down and goes passed the bottom bounds
        else if (transform.position.y < bounds.yMin && rigidbody2DComponent.velocity.y < 0) {
            WraparoundY();
        }

    }

    void WraparoundX() {
        transform.position = new Vector3(-transform.position.x, transform.position.y, transform.position.z);
    }
    void WraparoundY() {
        transform.position = new Vector3(transform.position.x, -transform.position.y, transform.position.z);
    }

}

