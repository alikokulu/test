﻿using UnityEngine;

public class Laser : MonoBehaviour {

    public float lifeSpan = 3f;
    public float speed = 30f;
    public Vector3 translatePosition;

    void Start() {
        // destroy this game object if it has reached its lifespan
        Invoke("Death", lifeSpan);
    }

    // Update is called once per frame
    void Update() {

        // Transform.Translate takes into account the rotation applied to the game object.
        // Because the ship's rotation has been applied to the laser when it was instantiated, 
        // we simply need to translate the laser along the X axis in local space -> Space.Self
        // note that Space.Self is the default - Space is an optional parameter
        transform.Translate(Vector3.right * speed * Time.deltaTime, Space.Self);
    }

    void Death() {
        Destroy(gameObject);
    }
}
