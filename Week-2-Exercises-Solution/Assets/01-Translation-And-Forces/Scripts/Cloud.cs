﻿using UnityEngine;

public class Cloud : MonoBehaviour {

    public Vector3 velocity = new Vector3(3, 0, 0);

    // Update is called once per frame
    void Update() {

        // move the cloud using translation - update the position without the help of the Physics engine
        transform.Translate(velocity * Time.deltaTime);

        // wrap the cloud around 
        if (transform.position.x > 10) {
            transform.position = new Vector3(-10, transform.position.y);
        }
    }
	
	//hello!!!
}
