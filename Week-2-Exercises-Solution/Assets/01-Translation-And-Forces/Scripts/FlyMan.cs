﻿using UnityEngine;

public class FlyMan : MonoBehaviour {

    public float horizontalThrust;
    public float maxVerticalThrust;
    float verticalThrust;

    float horizontalAxis;

    Rigidbody2D rigidbody2DComponent;

    void Awake() {
        // get references to components
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    void Update() {

        // always capture input in Update()
        horizontalAxis = Input.GetAxis("Horizontal");

        if (Input.GetButton("Jump")) {
            verticalThrust = maxVerticalThrust;
        } else {
            verticalThrust = 0;
        }
    }

    void FixedUpdate() {
        rigidbody2DComponent.AddForce(new Vector2(horizontalAxis * horizontalThrust, verticalThrust));
    }

}
